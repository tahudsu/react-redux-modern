import { selectSong } from "../actions";
import { combineReducers } from "redux";

const songsReducer = () => {
  return [
    { title: 'No scrubs', duration: '4:05'},
    { title: 'All Star', duration: '3:15'},
    { title: 'Starway to heaven', duration: '12:07' },
    { title: 'highway to hell', duration: '8:07' }
  ]
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }

  return selectSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
});
