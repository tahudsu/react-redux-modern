import React from 'react';
import LanguageContext from '../contexts/LanguageContext';


class Button extends React.Component {
  // static contextType = LanguageContext;
  
  render() {
    console.log(this.context);
    // const text = this.context === 'english' ? 'Submit' : 'Voorleggen';
    return (
      <button className="ui primary">
      <LanguageContext.Consumer>
        { ({language }) => language === 'english' ? 'Submit' : 'Voorleggen' }
      </LanguageContext.Consumer>
      </button>
    );
  }
}

// Button.contextType = LanguageContext;

export default Button;