import jsonPlaceholder from "../apis/jsonPlaceholder"
import _ from 'lodash';
export const fetchPosts = () => {
  return async (dispatch) => {
    const response = await jsonPlaceholder.get('/posts');
    
    dispatch({ type: 'FETCH_POSTS', payload: response.data });
  }
}

/* export const fetchUser = (id) => {
  return dispatch => {
    _fetchUser(id, dispatch);
  }
}

const _fetchUser = _.memoize(async (id, dispatch) => {
  const response = await jsonPlaceholder.get(`/users/${id}`);

  dispatch({ type: 'FETCH_USER', payload: response.data });
}); */

export const fetchUser = id => async dispatch => {
  const response = await jsonPlaceholder.get(`/users/${id}`);

  dispatch({ type: 'FETCH_USER', payload: response.data });
}

export const fetchPostsAndUsers = () => async (dispatch, getState) => {
  console.log('about to fetch posts');

  // call fetchPosts
  await dispatch(fetchPosts());
  //console.log('fetched psots');
  // get all posts data
  // console.error(getState().posts);
  // filter userId from object and return uniques
  // const userIds = _.uniq(_.map(getState().posts, 'userId'));
  // take all ids and dispatch from fetchUser by Id
  // userIds.forEach(id => dispatch(fetchUser(id)));

  // refactor all other code to this
  _.chain(getState().posts)
    .map('userId')
    .uniq()
    .forEach(id => dispatch(fetchUser(id)))
    .value();
};