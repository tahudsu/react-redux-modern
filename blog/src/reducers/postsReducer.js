// must return any values besides 'undefined
// produces 'state, or data to be used inside of our app using only
// previous state and the action
// must not return reach 'out of itself' to decide what value to return (reducers are pure, only works with method params)
// must not mutate its input 'state' argument, state can't change inside this method
// basicamente el state, se debe modificar como una copia blanda del mismo, por ejemplo 
// [ ...state, color] y no state.push(color), con el primero, creamos un nuevo objeto, mientras en el segundo, estamos modificando el objeto existente
// state.filter(color => color === 'blue') vs state.pop(color), puedes usar lodash
export default (state = [], action) => {
  switch (action.type) {
    case 'FETCH_POSTS':
      return action.payload;
    default:
      return state;
  }
};